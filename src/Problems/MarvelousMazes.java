package Problems;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Solves and prints the 
 * <a href = "http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=386">
 * MarvelousMazes</a> problem.
 *
 * @author Michael Lambert <michaelslambertcs@gmail.com>
 * @date 2014-4-3
 */
public class MarvelousMazes {

    private final String FILE_NAME = "C:\\Users\\lamby_000\\Documents\\Computer Science\\Projects\\Competition Problems\\src\\InputFiles\\MarvelousMazes.txt";
    private Scanner input;

    /**
     * Character which represents a space in the maze.
     */
    public static final char BLANK_CHAR = 'b';
    /**
     * Character which represents a new line in the maze.
     */
    public static final char LINE_CHAR = '!';
    //We read input one line at a time with line breaks dropped,
    //so we don't need to check them as a char for the actual maze translation.

    /**
     * Translates and prints a line of input, ignoring invalid characters. Will
     * print a blank line if input is empty.
     *
     * Notes: Since this system is designed to handle new lines in input
     * reading, new lines at the end of inputLine will be ignored.
     *
     * @param inputLine the line of maze input to be translated.
     */
    public void translateInput(String inputLine) {
        char[] buffer = inputLine.toCharArray();

        int i = 0, sum = 0; //Will contain our sum for consecutive integers.
        char next; //Will contain the next char to be read.
        StringBuilder mazeLine = new StringBuilder();

        while (i != buffer.length) {
            next = buffer[i];

            if (Character.isDigit(next)) {
                sum += next - '0';
            } else if (next == BLANK_CHAR) {
                while (sum != 0) {
                    mazeLine.append(" ");
                    --sum;
                }
            } else if (next == LINE_CHAR) {
                mazeLine.append('\n');
            } else if (Character.isUpperCase(next) || next == '*') {
                while (sum != 0) {
                    mazeLine.append(next);
                    --sum;
                }
            }
            ++i;
        }

        System.out.println(mazeLine.toString());
    }

    public void run() {
        try {
            input = new Scanner(new File(FILE_NAME));
            while (input.hasNextLine()) {
                String nextLine = input.nextLine();

                /* We automatically get a new line between mazes because of the
                 * guaranteed blank line between two input cases and the 
                 * fact that translateInput() prints a blank line when passed an
                 * empty string. */
                translateInput(nextLine);
            }
        } catch (FileNotFoundException f) {
            System.out.println("Input file not found.");
        }
    }

}
