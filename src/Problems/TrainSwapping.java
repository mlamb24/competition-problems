package Problems;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author Michael Lambert <michaelslambertcs@gmail.com>
 * @date 2014-3-26
 */
public class TrainSwapping {

    private final String FILE_NAME = "C:\\Users\\lamby_000\\Documents\\Computer Science\\Projects\\Competition Problems\\src\\InputFiles\\Trainswapping.txt";
    private int[] trains;
    private Scanner input;

    /**
     * Initializes the array for each case.
     */
    public void getTrains() {
        trains = new int[input.nextInt()];
        for (int i = 0; i < trains.length; i++) {
            trains[i] = input.nextInt();
        }
    }

    /**
     * Swaps train i and j.
     *
     * @param i the first train to be swapped.
     * @param j the second train to be swapped.
     */
    public void swap(int i, int j) {
        int temp = trains[i];
        trains[i] = trains[j];
        trains[j] = temp;
    }

    /**
     * Sorts the trains in ascending order using Bubble Sort.
     */
    public void sortTrains() {
        boolean swapped = true;
        int swapCount = 0;
        while (swapped) {
            swapped = false;
            for (int i = 0; trains.length - 1 > i; i++) {
                if (trains[i] > trains[i + 1]) {
                    swapped = true;
                    ++swapCount;
                    swap(i, i + 1);
                }
            }
        }
        System.out.printf("Optimal train swapping takes %d swaps.", swapCount);
    }

    /**
     * Runs the train problem with the given input file, set in source code.
     */
    public void run() {
        try {
            input = new Scanner(new File(FILE_NAME));

            int cases = input.nextInt();
            boolean firstIter = true;

            while (0 != cases--) {
                getTrains();
                if (!firstIter) {
                    System.out.println();
                } else {
                    firstIter = false;
                }
                sortTrains();
            }
        } catch (FileNotFoundException f) {
            System.out.println("Input file not found.");
        }
    }
}
