package Problems;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author Michael Lambert <michaelslambertcs@gmail.com>
 * @date 2014-03-20
 */
public class Interpreter {
    private Scanner input;
    
    private final String FILENAME = "C:\\Users\\lamby_000\\Documents\\Computer Science\\Projects\\Competition Problems\\src\\InputFiles\\Interpreter.txt";
    private final int REGSIZE = 10;
    private final int RAMSIZE = 1000;
    private int[] register = new int[REGSIZE];
    private Word[] ram = new Word[RAMSIZE];
    
    public void init() {
        register = new int[REGSIZE];
        for (int i = 0; RAMSIZE > i; i++) {
            ram[i] = new Word(0);
        }
    }
    
    public void getInput() {
        String nextLine = input.nextLine();
        int counter = 0;
        while(!nextLine.equals("")) {
            if(input.hasNextLine()) {
                ram[counter++] = new Word(nextLine);
                nextLine = input.nextLine();
            }
            else
                break;
        }
    }
    
    public void countInstructions() {
        int counter = 0;
        int ramPointer = 0;
        boolean loop = true;
        while(loop) {
            ++counter;
            Word cur = ram[ramPointer];
            switch(cur.getNum(0)) {
                case 1:
                    loop = false;
                    break;
                case 2:
                    register[cur.getNum(1)] = cur.getNum(2);
                    break;
                case 3:
                    register[cur.getNum(1)] += cur.getNum(2);
                    register[cur.getNum(1)] %= 1000;
                    break;
                case 4:
                    register[cur.getNum(1)] *= cur.getNum(2);
                    register[cur.getNum(1)] %= 1000;
                    break;
                case 5:
                    register[cur.getNum(1)] = register[cur.getNum(2)];
                    break;
                case 6:
                    register[cur.getNum(1)] += register[cur.getNum(2)];
                    register[cur.getNum(1)] %= 1000;
                    break;
                case 7:
                    register[cur.getNum(1)] *= register[cur.getNum(2)];
                    register[cur.getNum(1)] %= 1000;
                    break;
                case 8:
                    register[cur.getNum(1)] = ram[register[cur.getNum(2)]].getNum();
                    break;                    
                case 9:
                    ram[register[cur.getNum(2)]] = new Word(register[cur.getNum(1)]);
                    break;
                case 0:
                    if(register[cur.getNum(2)]!=0)
                        ramPointer = register[cur.getNum(1)]-1;
                    break;
                default:
                    System.out.println("Invalid number.");
                    break;
            }
            ++ramPointer;
        }
        System.out.println(counter);
    }
    
    public void run() {
        try {
            input = new Scanner(new File(FILENAME));
        } catch (FileNotFoundException fnf) {
            System.out.println("File not found");
        }
        int cases = 0;
        if(input.hasNextInt())
            cases = input.nextInt();
        else
            System.out.println("Number of cases not found");
        input.nextLine();
        input.nextLine();
        for(int i = 0; i < cases; i++) {
            if(i != 0) {
                System.out.println();
            }
            init();
            getInput();
            countInstructions();
        }
    }
    
    private class Word {
        int nums[] = new int[3];
        
        Word(String s) {
            nums[0] = (s.charAt(0) - '0');
            nums[1] = (s.charAt(1) - '0');
            nums[2] = (s.charAt(2) - '0');
        }
                
        Word(int num) {
            nums[0] = (num / 100);
            nums[1] = (num % 100) / 10;
            nums[2] = num % 10;
        }
        
        int getNum() {
            return nums[0] * 100 + nums[1] * 10 + nums[2];
        }
        
        int getNum(int i) {
            return nums[i];
        }
    }
}
