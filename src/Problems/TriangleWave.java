package Problems;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author Michael Lambert <michaelslambertcs@gmail.com>
 * @date 2014-4-2
 */
public class TriangleWave {
    private final String FILE_NAME = "C:\\Users\\lamby_000\\Documents\\Computer Science\\Projects\\Competition Problems\\src\\InputFiles\\TriangleWave.txt";
    private Scanner input;
    private StringBuilder[] triangles = new StringBuilder[10];
    
    /**
     * There are only 9 triangles to make, so we can easily 
     * generate the triangles in advance.
     */
    public void makeTriangles() {
        triangles[0] = new StringBuilder("\n");
        for(int i = 1; 10 > i; i++) {
            triangles[i] = new StringBuilder();
            boolean peaked = false;
            int j = 1;
            while (j!= 0) {
                for (int k = 0; k<j; k++) {
                    triangles[i].append(j);
                }
                triangles[i].append("\n");
                if (j==i)
                    peaked = true;
                if (peaked)
                    --j;
                else
                    ++j;
            }
            triangles[i].append("\n");
        }
    }
    
    /**
     * Prints freq pregenerated triangle of amp height 
     * @param amp The height of the triangle
     * @param freq The number of triangles to print.
     */
    public void printWave(int amp, int freq) {
        for (int i = 0; i < freq; i++) {
            System.out.print(triangles[amp].toString());
        }
    }
    
    /**
     * Runs the triangle wave printer for the input file given internally.
     */
    public void run() {
        makeTriangles();
        try {
            input = new Scanner(new File(FILE_NAME));
            int cases = input.nextInt();
            for (; 0 < cases; cases--) {
                int amp = input.nextInt();
                int freq = input.nextInt();
                printWave(amp, freq);
            }
        } catch (FileNotFoundException f) {
            System.out.println("Input file not found.");
        }
    }
}
