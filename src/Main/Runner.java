package Main;

import Problems.*;

/**
 * Main runner file which times the execution of the problem.
 *
 * @author Michael Lambert <michaelslambertcs@gmail.com>
 * @date 2014-03-19
 */
public class Runner {

    /**
     * Prints the solution and runtime for whatever problem is called.
     */
    public static void main(String[] args) {
        long start = System.nanoTime();
        new MarvelousMazes().run();
        long end = System.nanoTime();
        System.out.printf("\nRuntime: %dns (%dms)\n", end - start, (end - start) / 1000000);
    }

}
