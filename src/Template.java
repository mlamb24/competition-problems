//package Problems;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * The template file for a problem with input reader.
 * 
 * @author Michael Lambert <michaelslambertcs@gmail.com>
 */
public class Template {

    private final String FILE_NAME = "C:\\Users\\lamby_000\\Documents\\Computer Science\\Projects\\Competition Problems\\src\\InputFiles\\CHANGEME.txt";
    private Scanner input;

    public void run() {
        try {
            input = new Scanner(new File(FILE_NAME));
        } catch (FileNotFoundException f) {
            System.out.println("Input file not found.");
        }
    }

}
